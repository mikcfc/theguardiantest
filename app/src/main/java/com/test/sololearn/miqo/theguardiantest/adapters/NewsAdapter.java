package com.test.sololearn.miqo.theguardiantest.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.test.sololearn.miqo.theguardiantest.activities.ArticleActivity;
import com.test.sololearn.miqo.theguardiantest.activities.MainActivity;
import com.test.sololearn.miqo.theguardiantest.R;
import com.test.sololearn.miqo.theguardiantest.models.NewsResponse;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private ArrayList<NewsResponse.ResponseBean.ResultsBean> mNewsList;

    public NewsAdapter() {
        mNewsList = new ArrayList<>();
    }

    public void setData(List<NewsResponse.ResponseBean.ResultsBean> newsItems) {
        mNewsList.clear();
        mNewsList.addAll(newsItems);
        notifyDataSetChanged();
    }

    public void addMore(List<NewsResponse.ResponseBean.ResultsBean> items) {
        mNewsList.addAll(items);
        notifyItemRangeInserted(mNewsList.size() - items.size(), items.size());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(holder.itemView.getContext())
                .load(getItem(position).getFields() != null ? getItem(position).getFields().getThumbnail() : holder.itemView.getContext().getDrawable(R.drawable.image_placeholder))
                .apply(RequestOptions.placeholderOf(R.drawable.image_placeholder))
                .into(holder.mImageNews);
        holder.mTextTitle.setText(getItem(position).getWebTitle());
        holder.mTextCategory.setText(getItem(position).getSectionName());
    }

    @Override
    public int getItemCount() {
        return mNewsList.size();
    }

    private NewsResponse.ResponseBean.ResultsBean getItem(int position) {
        return mNewsList.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageNews;
        private TextView mTextTitle;
        private TextView mTextCategory;

        @SuppressLint("RestrictedApi")
        ViewHolder(View itemView) {
            super(itemView);
            mImageNews = itemView.findViewById(R.id.image_news);
            mTextTitle = itemView.findViewById(R.id.text_title);
            mTextCategory = itemView.findViewById(R.id.text_category);
            itemView.setClipToOutline(true);
            itemView.setOnClickListener((View v) -> {
                Intent intent = new Intent(itemView.getContext(), ArticleActivity.class);
                if (getItem(getAdapterPosition()).getFields() != null) {
                    intent.putExtra("image_url", getItem(getAdapterPosition()).getFields().getThumbnail());
                }
                intent.putExtra("title", getItem(getAdapterPosition()).getWebTitle());
                intent.putExtra("category", getItem(getAdapterPosition()).getSectionName());
                intent.putExtra("news_url", getItem(getAdapterPosition()).getWebUrl());
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(((MainActivity) itemView.getContext()), mImageNews, ViewCompat.getTransitionName(mImageNews));
                itemView.getContext().startActivity(intent, options.toBundle());
            });
        }
    }
}
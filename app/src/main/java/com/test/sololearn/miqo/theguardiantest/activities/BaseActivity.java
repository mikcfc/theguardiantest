package com.test.sololearn.miqo.theguardiantest.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.test.sololearn.miqo.theguardiantest.R;
import com.test.sololearn.miqo.theguardiantest.TheGuardianApplication;

public abstract class BaseActivity extends AppCompatActivity {

    private boolean mConnectionLostOnce;

    private BroadcastReceiver mConnectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (TheGuardianApplication.getInstance().isNetworkAvailable() && mConnectionLostOnce) {
                onConnectionRestored();
            }

            if (!TheGuardianApplication.getInstance().isNetworkAvailable()) {
                onConnectionLost();
            }
        }
    };

    public void onConnectionLost() {
        Snackbar snackbar = Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Connection lost", Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorRed));
        snackbar.show();
        mConnectionLostOnce = true;
    }

    public void onConnectionRestored() {
        Snackbar snackbar = Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), "Connection restored", Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreen));
        snackbar.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mConnectionLostOnce = false;

        try {
            if (mConnectionReceiver != null) {
                unregisterReceiver(mConnectionReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        registerReceiver(mConnectionReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onStop() {
        try {
            if (mConnectionReceiver != null) {
                unregisterReceiver(mConnectionReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onStop();
    }


}
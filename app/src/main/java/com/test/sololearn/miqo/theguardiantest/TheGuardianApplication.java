package com.test.sololearn.miqo.theguardiantest;

import android.app.Application;
import android.app.NotificationManager;
import android.arch.lifecycle.LifecycleObserver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.test.sololearn.miqo.theguardiantest.service.NewsService;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class TheGuardianApplication extends Application implements LifecycleObserver{

    private static TheGuardianApplication sTheGuardianApplication;
    private ConnectivityManager mConnectivityManager;
    public NotificationManager mNotificationManager;
    public Intent serviceIntent;

    public static TheGuardianApplication getInstance() {
        return sTheGuardianApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sTheGuardianApplication = this;
        mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        serviceIntent = new Intent(this, NewsService.class);
        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder().name("theguardian.realm").schemaVersion(0).build();
        Realm.setDefaultConfiguration(realmConfig);
    }

    public boolean isNetworkAvailable() {
        return mConnectivityManager.getActiveNetworkInfo() != null;
    }

    public void startService() {
        serviceIntent.setAction(NewsService.START_SERVICE);
        startService(serviceIntent);
    }

    public void stopService() {
        serviceIntent.setAction(NewsService.STOP_SERVICE);
        startService(serviceIntent);
    }
}

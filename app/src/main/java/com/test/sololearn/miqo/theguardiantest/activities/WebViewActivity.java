package com.test.sololearn.miqo.theguardiantest.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;

import com.test.sololearn.miqo.theguardiantest.R;

public class WebViewActivity extends BaseActivity {

    private WebView mWebView;

    @SuppressLint("SetJavaScriptEnabled")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        mWebView = findViewById(R.id.web_view);
        mWebView.getSettings().setJavaScriptEnabled(true);
        loadData();
    }

    private void loadData() {
        if (getIntent().getExtras() != null) {
            mWebView.loadUrl(getIntent().getExtras().getString("news_url"));
        } else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionRestored() {
        super.onConnectionRestored();
        loadData();
    }
}
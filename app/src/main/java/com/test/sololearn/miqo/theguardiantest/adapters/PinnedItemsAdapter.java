package com.test.sololearn.miqo.theguardiantest.adapters;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.test.sololearn.miqo.theguardiantest.activities.ArticleActivity;
import com.test.sololearn.miqo.theguardiantest.activities.MainActivity;
import com.test.sololearn.miqo.theguardiantest.R;
import com.test.sololearn.miqo.theguardiantest.models.ItemPinned;

import java.util.ArrayList;

import io.realm.OrderedRealmCollection;

public class PinnedItemsAdapter extends RecyclerView.Adapter<PinnedItemsAdapter.ViewHolder> {

    private ArrayList<ItemPinned> mListItemsPinned;

    public PinnedItemsAdapter() {
        mListItemsPinned = new ArrayList<>();
    }

    public void setData(@Nullable OrderedRealmCollection<ItemPinned> pinnedItems) {
        mListItemsPinned.clear();
        mListItemsPinned.addAll(pinnedItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pinned, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(holder.itemView.getContext())
                .load(getItem(position).getImageUrl() != null ? getItem(position).getImageUrl() : holder.itemView.getContext().getDrawable(R.drawable.image_placeholder))
                .apply(RequestOptions.placeholderOf(R.drawable.image_placeholder))
                .into(holder.mImageNews);
    }

    @Override
    public int getItemCount() {
        return mListItemsPinned.size();
    }

    private ItemPinned getItem(int position) {
        return mListItemsPinned.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageNews;

        ViewHolder(View itemView) {
            super(itemView);
            mImageNews = itemView.findViewById(R.id.image_news);
            itemView.setClipToOutline(true);
            itemView.setOnClickListener((View v) -> {
                Intent intent = new Intent(itemView.getContext(), ArticleActivity.class);
                intent.putExtra("image_url", getItem(getAdapterPosition()).getImageUrl());
                intent.putExtra("title", getItem(getAdapterPosition()).getTitle());
                intent.putExtra("category", getItem(getAdapterPosition()).getCategory());
                intent.putExtra("news_url", getItem(getAdapterPosition()).getUrl());
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(((MainActivity) itemView.getContext()), mImageNews, ViewCompat.getTransitionName(mImageNews));
                itemView.getContext().startActivity(intent, options.toBundle());
            });
        }
    }
}
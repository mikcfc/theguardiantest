package com.test.sololearn.miqo.theguardiantest.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsResponse {

    @SerializedName("response")
    private ResponseBean mResponseBean;

    public ResponseBean getResponseBean() {
        return mResponseBean;
    }

    public static class ResponseBean {

        @SerializedName("results")
        private List<ResultsBean> mResults;

        public List<ResultsBean> getResults() {
            return mResults;
        }

        public static class ResultsBean {

            @SerializedName("sectionName")
            private String mSectionName;

            @SerializedName("webTitle")
            private String mWebTitle;

            @SerializedName("webUrl")
            private String mWebUrl;

            @SerializedName("fields")
            private FieldsBean mFields;

            public String getSectionName() {
                return mSectionName;
            }

            public String getWebTitle() {
                return mWebTitle;
            }

            public String getWebUrl() {
                return mWebUrl;
            }

            public FieldsBean getFields() {
                return mFields;
            }

            public static class FieldsBean {

                @SerializedName("thumbnail")
                private String mThumbnail;

                public String getThumbnail() {
                    return mThumbnail;
                }
            }
        }
    }
}

package com.test.sololearn.miqo.theguardiantest.network;

import com.test.sololearn.miqo.theguardiantest.listeners.CallbackListener;
import com.test.sololearn.miqo.theguardiantest.models.NewsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsManager {

    private static NewsManager sNewsManager;

    private NewsResource mNewsResource;

    public static NewsManager getInstance() {
        if (sNewsManager == null) {
            sNewsManager = new NewsManager();
        }

        return sNewsManager;
    }

    private NewsManager() {
        mNewsResource = ResourceFactory.createResource(NewsResource.class);
    }

    public void receiveNews(String query, String resultsFormat, int page, String showFields, String apiKey, CallbackListener<Response<NewsResponse>> callback) {
        mNewsResource.receiveNews(query, resultsFormat, page, showFields, apiKey).enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> responseCall, Response<NewsResponse> response) {
                if (200 == response.code()) {
                    callback.success(response);
                } else {
                    callback.failure(new Throwable());
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                callback.failure(t);
            }
        });
    }
}

package com.test.sololearn.miqo.theguardiantest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.test.sololearn.miqo.theguardiantest.R;
import com.test.sololearn.miqo.theguardiantest.models.ItemPinned;

import io.realm.Realm;
import io.realm.RealmResults;

public class ArticleActivity extends BaseActivity implements View.OnClickListener {

    private boolean mLoaded;
    private ImageView mImageView;
    private TextView mTextTitle;
    private TextView mTextCategory;
    private Button mButtonMore;
    private Button mButtonPinToMainPage;
    private Realm mRealm;
    private RealmResults<ItemPinned> mPinnedRealmResults;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        mImageView = findViewById(R.id.image_news);
        mTextTitle = findViewById(R.id.text_title);
        mTextCategory = findViewById(R.id.text_category);
        mButtonMore = findViewById(R.id.button_more);
        mButtonPinToMainPage = findViewById(R.id.button_pin_to_main_page);
        loadData();
    }

    private void loadData() {
        mLoaded = true;
        if (getIntent().getExtras() != null) {
            Glide.with(this)
                    .load(getIntent().getExtras().getString("image_url") != null ? getIntent().getExtras().getString("image_url") : getDrawable(R.drawable.image_placeholder))
                    .apply(RequestOptions.placeholderOf(R.drawable.image_placeholder))
                    .into(mImageView);
        }
        mTextTitle.setText(getIntent().getExtras().getString("title"));
        setTitle(getIntent().getExtras().getString("title"));
        mTextCategory.setText(String.format("Category: %s", getIntent().getExtras().getString("category")));
        mButtonMore.setOnClickListener(this);
        mButtonPinToMainPage.setOnClickListener(this);
    }

    @Override
    public void onConnectionRestored() {
        super.onConnectionRestored();

        if (!mLoaded) {
            loadData();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_more:
                if (getIntent().getExtras() != null) {
                    Intent intent = new Intent(this, WebViewActivity.class);
                    intent.putExtra("news_url", getIntent().getExtras().getString("news_url"));
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.button_pin_to_main_page:
                if (getIntent().getExtras() != null) {
                    for (int i = 0; i < mPinnedRealmResults.size(); i++) {
                        if (mPinnedRealmResults.get(i).getImageUrl() != null) {
                            if (mPinnedRealmResults.get(i).getImageUrl().equals(getIntent().getExtras().getString("image_url"))) {
                                Toast.makeText(this, "This page is already pinned", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        } else {
                            break;
                        }
                    }

                    mRealm.beginTransaction();
                    ItemPinned itemPinned = mRealm.createObject(ItemPinned.class);
                    itemPinned.setImageUrl(getIntent().getExtras().getString("image_url"));
                    itemPinned.setCategory(getIntent().getExtras().getString("category"));
                    itemPinned.setUrl(getIntent().getExtras().getString("news_url"));
                    itemPinned.setTitle(getIntent().getExtras().getString("title"));
                    mRealm.commitTransaction();
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mRealm = Realm.getDefaultInstance();
        mPinnedRealmResults = mRealm.where(ItemPinned.class).findAll();
    }

    @Override
    protected void onStop() {
        mRealm.close();
        super.onStop();
    }
}
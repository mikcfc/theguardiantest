package com.test.sololearn.miqo.theguardiantest.network;

import com.test.sololearn.miqo.theguardiantest.models.NewsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsResource {

    @GET("/search")
    Call<NewsResponse> receiveNews(@Query("q") String query,
                                   @Query("format") String resultsFormat,
                                   @Query("page") int age,
                                   @Query("show-fields") String showFields,
                                   @Query("api-key") String apiKey);
}

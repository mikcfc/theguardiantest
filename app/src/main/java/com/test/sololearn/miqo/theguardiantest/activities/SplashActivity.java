package com.test.sololearn.miqo.theguardiantest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.test.sololearn.miqo.theguardiantest.R;

public class SplashActivity extends BaseActivity {

    private static final int SPLASH_DELAY = 2000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(() -> {
            SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainActivity.class));
            overridePendingTransition(0, 0);
            finish();
        }, SPLASH_DELAY);
    }
}

package com.test.sololearn.miqo.theguardiantest.activities;

import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.test.sololearn.miqo.theguardiantest.listeners.EndlessRecyclerViewScrollListener;
import com.test.sololearn.miqo.theguardiantest.R;
import com.test.sololearn.miqo.theguardiantest.utils.SpacesItemDecoration;
import com.test.sololearn.miqo.theguardiantest.TheGuardianApplication;
import com.test.sololearn.miqo.theguardiantest.models.ItemPinned;
import com.test.sololearn.miqo.theguardiantest.adapters.NewsAdapter;
import com.test.sololearn.miqo.theguardiantest.adapters.PinnedItemsAdapter;
import com.test.sololearn.miqo.theguardiantest.models.NewsResponse;
import com.test.sololearn.miqo.theguardiantest.listeners.CallbackListener;
import com.test.sololearn.miqo.theguardiantest.network.NewsManager;

import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Response;

import static com.test.sololearn.miqo.theguardiantest.utils.Constants.API_KEY;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.GET_NEW_DATA_DELAY;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.QUERY_TEXT_CHANGED_DELAY;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.RESULTS_FORMAT;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.SHOW_FIELDS;

public class MainActivity extends BaseActivity {

    private RecyclerView mListNews;
    private TextView mTextNoPinnedItems;
    private NewsAdapter mNewsAdapter;
    private PinnedItemsAdapter mPinnedItemsAdapter;
    private SwipeRefreshLayout mLayoutSwipeRefresh;
    private Timer mTimerCheckServer;
    private TextView mViewNoInternet;
    private LinearLayoutManager mLinearLayoutManager;
    private StaggeredGridLayoutManager mGridLayoutManager;
    private SearchView mSearchView;
    private EndlessRecyclerViewScrollListener mEndlessScrollListener;
    private boolean mLoaded;
    private Handler mRequestHandler = new Handler();
    private ProgressBar mProgressBar;
    private Realm mRealm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("News feed");
        mListNews = findViewById(R.id.list_news);
        RecyclerView listPinnedItems = findViewById(R.id.list_pinned_items);
        mViewNoInternet = findViewById(R.id.view_no_internet);
        mLayoutSwipeRefresh = findViewById(R.id.layout_swipe_to_refresh);
        mTextNoPinnedItems = findViewById(R.id.text_no_pinned_items);
        mProgressBar = findViewById(R.id.progress_bar);
        mNewsAdapter = new NewsAdapter();
        mPinnedItemsAdapter = new PinnedItemsAdapter();
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        SpacesItemDecoration decoration = new SpacesItemDecoration(16);
        mListNews.addItemDecoration(decoration);
        mListNews.setLayoutManager(mGridLayoutManager);
        listPinnedItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        listPinnedItems.setAdapter(mPinnedItemsAdapter);
        mListNews.setAdapter(mNewsAdapter);
        mEndlessScrollListener = new EndlessRecyclerViewScrollListener(mGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMore(String.valueOf(mSearchView.getQuery()), page);
            }
        };
        mListNews.addOnScrollListener(mEndlessScrollListener);
        mLayoutSwipeRefresh.setOnRefreshListener(() -> {
            if (mSearchView.getQuery().length() == 0) {
                getNews(null);
            } else {
                getNews(String.valueOf(mSearchView.getQuery()).trim());
            }
        });
        getNews(null);
    }

    private void getPinnedItems() {
        RealmResults<ItemPinned> pinnedRealmResults = mRealm.where(ItemPinned.class).findAll();

        if (pinnedRealmResults.size() > 0) {
            mTextNoPinnedItems.setVisibility(View.GONE);
        }

        mPinnedItemsAdapter.setData(pinnedRealmResults);
    }

    private void getNews(String query) {
        if (query == null || query.length() > 2) {
            mProgressBar.setVisibility(View.VISIBLE);
            mListNews.setVisibility(View.GONE);
            NewsManager.getInstance().receiveNews(query, RESULTS_FORMAT, 1, SHOW_FIELDS, API_KEY, new CallbackListener<Response<NewsResponse>>() {
                @Override
                public void success(Response<NewsResponse> response) {
                    mLoaded = true;
                    mProgressBar.setVisibility(View.GONE);
                    mListNews.setVisibility(View.VISIBLE);
                    if (mViewNoInternet.getVisibility() == View.VISIBLE) {
                        mViewNoInternet.setVisibility(View.GONE);
                    }
                    mLayoutSwipeRefresh.setRefreshing(false);
                    mNewsAdapter.setData(response.body().getResponseBean().getResults());
                }

                @Override
                public void failure(Throwable body) {
                    if (!TheGuardianApplication.getInstance().isNetworkAvailable() && !mLoaded) {
                        mViewNoInternet.setVisibility(View.VISIBLE);
                    } else {
                        Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong", Snackbar.LENGTH_LONG);
                    }
                    mLayoutSwipeRefresh.setRefreshing(false);
                }
            });
        }
    }

    private void loadMore(String query, int page) {
        NewsManager.getInstance().receiveNews(query, RESULTS_FORMAT, page, SHOW_FIELDS, API_KEY, new CallbackListener<Response<NewsResponse>>() {
            @Override
            public void success(Response<NewsResponse> response) {
                if (mViewNoInternet.getVisibility() == View.VISIBLE) {
                    mViewNoInternet.setVisibility(View.GONE);
                }
                if (mNewsAdapter != null) {
                    mEndlessScrollListener.setLoading(false);
                    if (response.body().getResponseBean().getResults().size() > 0) {
                        mNewsAdapter.addMore(response.body().getResponseBean().getResults());
                    }
                }
            }

            @Override
            public void failure(Throwable body) {
                mLayoutSwipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onConnectionRestored() {
        super.onConnectionRestored();
        if (mViewNoInternet.getVisibility() == View.VISIBLE) {
            mViewNoInternet.setVisibility(View.GONE);
        }

        if (!mLoaded) {
            if (mSearchView.getQuery().length() == 0) {
                getNews(null);
            } else {
                getNews(String.valueOf(mSearchView.getQuery()).trim());
            }
        }
    }

    private void startListener() {
        mTimerCheckServer = new Timer();
        mTimerCheckServer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    if (mSearchView.getQuery().length() == 0) {
                        getNews(null);
                    } else {
                        getNews(String.valueOf(mSearchView.getQuery()).trim());
                    }
                });
            }
        }, GET_NEW_DATA_DELAY, GET_NEW_DATA_DELAY);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        if (searchItem != null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }

        if (mSearchView != null) {
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    getNews(query.trim());
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    mRequestHandler.removeCallbacksAndMessages(null);
                    mRequestHandler.postDelayed(() -> getNews(newText.trim()), QUERY_TEXT_CHANGED_DELAY);
                    return true;
                }
            });
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mRealm = Realm.getDefaultInstance();
        getPinnedItems();
        startListener();
        TheGuardianApplication.getInstance().stopService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRealm.close();
        if (mTimerCheckServer != null) {
            mTimerCheckServer.cancel();
            mTimerCheckServer = null;
        }

        TheGuardianApplication.getInstance().startService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_layout_type:
                int lastPosition;
                if (mListNews.getLayoutManager() instanceof StaggeredGridLayoutManager) {
                    int[] lastVisibleItemPositions = ((StaggeredGridLayoutManager) mListNews.getLayoutManager()).findFirstCompletelyVisibleItemPositions(null);
                    lastPosition = lastVisibleItemPositions[0];
                    mListNews.setLayoutManager(mLinearLayoutManager);
                    mListNews.setAdapter(mNewsAdapter);
                    mListNews.scrollToPosition(lastPosition);
                    return true;
                }

                if (mListNews.getLayoutManager() instanceof LinearLayoutManager) {
                    lastPosition = ((LinearLayoutManager) mListNews.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                    mListNews.setLayoutManager(mGridLayoutManager);
                    mListNews.setAdapter(mNewsAdapter);
                    mListNews.scrollToPosition(lastPosition);
                    return true;
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package com.test.sololearn.miqo.theguardiantest.network;

import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.sololearn.miqo.theguardiantest.TheGuardianApplication;

import java.io.File;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.test.sololearn.miqo.theguardiantest.utils.Constants.DEFAULT_DATE_FORMAT;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.HEADER_CACHE_CONTROL;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.HEADER_PRAGMA;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.MAIN_URL;

public class ResourceFactory {

    private static Retrofit mRootResourceFactory;

    public static <T> T createResource(Class<T> resourceClass) {
        if (mRootResourceFactory == null) {
            try {
                final TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            @SuppressLint("TrustAllX509TrustManager")
                            @Override
                            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {

                            }

                            @SuppressLint("TrustAllX509TrustManager")
                            @Override
                            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {

                            }

                            @Override
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return new java.security.cert.X509Certificate[]{};
                            }
                        }
                };

                final SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                OkHttpClient okHttpClient = new OkHttpClient.Builder().sslSocketFactory(new TLSSocketFactory(sslContext.getSocketFactory()), (X509TrustManager) trustAllCerts[0]).hostnameVerifier((hostname, session) -> true)
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .readTimeout(10, TimeUnit.SECONDS)
                        .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        .addInterceptor(provideOfflineCacheInterceptor())
                        .addNetworkInterceptor(provideCacheInterceptor())
                        .cache(provideCache())
                        .build();
                Gson gson = new GsonBuilder().setLenient().setDateFormat(DEFAULT_DATE_FORMAT).create();
                mRootResourceFactory = new Retrofit.Builder()
                        .baseUrl(MAIN_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .client(okHttpClient)
                        .build();
            } catch (NoSuchAlgorithmException | KeyManagementException e) {
                e.printStackTrace();
            }
        }

        return mRootResourceFactory.create(resourceClass);
    }

    private static Cache provideCache() {
        Cache cache = null;

        try {
            cache = new Cache(new File(TheGuardianApplication.getInstance().getCacheDir(), "http-cache"), 10 * 1024 * 1024);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cache;
    }

    private static Interceptor provideCacheInterceptor() {
        return chain -> {
            CacheControl cacheControl;
            if (TheGuardianApplication.getInstance().isNetworkAvailable()) {
                cacheControl = new CacheControl.Builder().maxAge(0, TimeUnit.SECONDS).build();
            } else {
                cacheControl = new CacheControl.Builder().maxStale(7, TimeUnit.DAYS).build();
            }

            return chain.proceed(chain.request()).newBuilder()
                    .removeHeader(HEADER_PRAGMA)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                    .build();
        };
    }

    private static Interceptor provideOfflineCacheInterceptor() {
        return chain -> {
            Request request = chain.request();
            if (!TheGuardianApplication.getInstance().isNetworkAvailable()) {
                CacheControl cacheControl = new CacheControl.Builder().maxStale(7, TimeUnit.DAYS).build();
                request = request.newBuilder()
                        .removeHeader(HEADER_PRAGMA)
                        .removeHeader(HEADER_CACHE_CONTROL)
                        .cacheControl(cacheControl)
                        .build();
            }

            return chain.proceed(request);
        };
    }
}

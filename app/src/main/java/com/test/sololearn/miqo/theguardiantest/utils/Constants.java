package com.test.sololearn.miqo.theguardiantest.utils;

public class Constants {

    public static final int QUERY_TEXT_CHANGED_DELAY = 500;
    public static final int GET_NEW_DATA_DELAY = 30000;

    public static final String DEFAULT_DATE_FORMAT = "dd'-'MM'-'yyyy'T'HH':'mm':'ss";
    public static final String API_KEY = "b8aceec3-6938-4c6d-b2e1-d1793947ce57";
    public static final String MAIN_URL = "https://content.guardianapis.com";
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String SHOW_FIELDS = "thumbnail";
    public static final String RESULTS_FORMAT = "json";
    public static final String HEADER_PRAGMA = "Pragma";

}

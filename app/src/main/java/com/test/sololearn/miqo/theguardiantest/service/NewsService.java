package com.test.sololearn.miqo.theguardiantest.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.test.sololearn.miqo.theguardiantest.R;
import com.test.sololearn.miqo.theguardiantest.TheGuardianApplication;
import com.test.sololearn.miqo.theguardiantest.models.NewsResponse;
import com.test.sololearn.miqo.theguardiantest.listeners.CallbackListener;
import com.test.sololearn.miqo.theguardiantest.network.NewsManager;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Response;

import static com.test.sololearn.miqo.theguardiantest.utils.Constants.API_KEY;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.GET_NEW_DATA_DELAY;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.RESULTS_FORMAT;
import static com.test.sololearn.miqo.theguardiantest.utils.Constants.SHOW_FIELDS;

public class NewsService extends Service {

    public static final String START_SERVICE = "START_SERVICE";
    public static final String STOP_SERVICE = "STOP_SERVICE";

    private Timer mTimerCheckServer;
    private Handler mHandler = new Handler();

    public NewsService() {
        super();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            switch (Objects.requireNonNull(action)) {
                case START_SERVICE:
                    startService();
                    break;

                case STOP_SERVICE:
                    stopService();
                    break;
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void startService() {
        setApplicationToStatusBar();
        startListener();
    }

    private void stopService() {
        if (mTimerCheckServer != null) {
            mTimerCheckServer.cancel();
            mTimerCheckServer = null;
        }

        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void generateNotification() {
        Intent i = Objects.requireNonNull(getPackageManager().getLaunchIntentForPackage(getPackageName())).setPackage(null).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);
        NotificationManager notificationManager = TheGuardianApplication.getInstance().mNotificationManager;
        NotificationCompat.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel notificationChannel = new NotificationChannel("1", "The Guardian", importance);

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }

            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext(), "1");
        }

        builder.setContentText("The Guardian");
        builder.setContentTitle("Check out news");
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setPriority(Notification.PRIORITY_MAX);
        builder.setContentIntent(pendingIntent);
        if (notificationManager != null) {
            notificationManager.notify(2, builder.build());
        }
    }

    private void setApplicationToStatusBar() {
        Intent i = Objects.requireNonNull(getPackageManager().getLaunchIntentForPackage(getPackageName())).setPackage(null).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);
        NotificationManager notificationManager = TheGuardianApplication.getInstance().mNotificationManager;
        NotificationCompat.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel notificationChannel = new NotificationChannel("1", "The Guardian", importance);

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }

            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext(), "1");
        }

        builder.setContentText("The Guardian");
        builder.setContentTitle("Press to return");
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setPriority(Notification.PRIORITY_MAX);
        builder.setContentIntent(pendingIntent);
        Notification notification = builder.build();
        startForeground(1, notification);
    }

    private void runOnUiThread(Runnable runnable) {
        mHandler.post(runnable);
    }

    private void startListener() {
        mTimerCheckServer = new Timer();
        mTimerCheckServer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> getNews());
            }
        }, GET_NEW_DATA_DELAY, GET_NEW_DATA_DELAY);
    }

    private void getNews() {
        NewsManager.getInstance().receiveNews(null, RESULTS_FORMAT, 1, SHOW_FIELDS, API_KEY, new CallbackListener<Response<NewsResponse>>() {
            @Override
            public void success(Response<NewsResponse> response) {
                generateNotification();
            }

            @Override
            public void failure(Throwable body) {

            }
        });
    }
}

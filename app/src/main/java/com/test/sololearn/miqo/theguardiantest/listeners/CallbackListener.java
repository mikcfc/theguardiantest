package com.test.sololearn.miqo.theguardiantest.listeners;

public interface CallbackListener<T> {

    void success(T o);

    void failure(Throwable error);

}
